package cz.tom.movieDatabase.entities;

import java.util.List;
import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
public class Movie
    implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "title", nullable = false)
    private String title;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "director")
    private Person director;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "writer")
    private Person writer;

    @Column(name = "release_year")
    private Integer releaseYear;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Column(name = "main_star")
    private List<Person> mainStars;

    @Column(name = "genre")
    private String genre;

    @Column(name = "rating")
    private Float rating;

    @Column(name = "movie_type")
    private String movieType;

    public Movie(
        final String title,
        final Person director,
        final Person writer,
        final Integer releaseYear,
        final List<Person> mainStars,
        final String genre,
        final Float rating,
        final String movieType)
    {
        this.id = null;
        this.title = title;
        this.director = director;
        this.writer = writer;
        this.releaseYear = releaseYear;
        this.mainStars = mainStars;
        this.genre = genre;
        this.rating = rating;
        this.movieType = movieType;
    }
}
