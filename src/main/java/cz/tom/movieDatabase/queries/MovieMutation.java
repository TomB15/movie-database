package cz.tom.movieDatabase.queries;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cz.tom.movieDatabase.entities.Movie;
import cz.tom.movieDatabase.services.MovieService;

@Component
public class MovieMutation
    implements GraphQLMutationResolver
{
    @Autowired
    private MovieService movieService;

    public final Movie createMovie(
        final Movie movie)
    {
        return movieService.createMovie(movie);
    }
}
