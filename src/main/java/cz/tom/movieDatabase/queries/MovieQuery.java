package cz.tom.movieDatabase.queries;

import java.util.List;
import java.util.Optional;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cz.tom.movieDatabase.services.MovieService;
import cz.tom.movieDatabase.entities.Movie;

@Component
public class MovieQuery
    implements GraphQLQueryResolver
{
    @Autowired
    private MovieService service;

    public List<Movie> getMovies()
    {
        return service.getAllMovies();
    }

    public Optional<Movie> getMovie(
        final Integer id)
    {
        return service.getMovie(id);

    }
}
