package cz.tom.movieDatabase.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cz.tom.movieDatabase.entities.Movie;

@Repository
public interface MovieRepository
    extends JpaRepository<Movie, Integer>
{
}
