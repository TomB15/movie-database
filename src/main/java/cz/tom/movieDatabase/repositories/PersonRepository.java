package cz.tom.movieDatabase.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cz.tom.movieDatabase.entities.Person;

@Repository
public interface PersonRepository
    extends JpaRepository<Person, Integer>
{
}
