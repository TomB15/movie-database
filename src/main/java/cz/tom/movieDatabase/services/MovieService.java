package cz.tom.movieDatabase.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.tom.movieDatabase.entities.Movie;
import cz.tom.movieDatabase.repositories.MovieRepository;

@Service
public class MovieService
{
    private final MovieRepository repository;

    public MovieService(
        final MovieRepository repository)
    {
        this.repository = repository;
    }

    @Transactional
    public Movie createMovie(
        final Movie movie)
    {

        return repository.save(movie);
    }

    @Transactional(readOnly = true)
    public List<Movie> getAllMovies()
    {
        return repository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Movie> getMovie(
        final Integer id)
    {
        return repository.findById(id);
    }
}
