package cz.tom.movieDatabase.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.tom.movieDatabase.entities.Person;
import cz.tom.movieDatabase.repositories.PersonRepository;

@Service
public class PersonService
{
    private final PersonRepository personRepository;

    public PersonService(
        final PersonRepository personRepository)
    {
        this.personRepository = personRepository;
    }

    @Transactional
    public Person createPerson(
        final Person person)
    {
        return personRepository.save(person);
    }

    @Transactional(readOnly = true)
    public List<Person> getAllPerson()
    {
        return personRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Person> getPerson(
        final Integer id)
    {
        return personRepository.findById(id);
    }
}
