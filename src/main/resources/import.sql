-- insert persons
INSERT INTO public.person(first_name, last_name) VALUES ('Peter', 'Jackson');
INSERT INTO public.person(first_name, last_name) VALUES ('J.R.R.', 'Tolkein');
INSERT INTO public.person(first_name, last_name) VALUES ('Elijah', 'Wood');

-- insert movies
INSERT INTO public.movie(title, director, writer, release_year, genre, rating, movie_type) VALUES (  'Lord of the Rings: The Fellowship of the Ring',(SELECT id FROM public.person WHERE first_name = 'Peter' AND last_name = 'Jackson'), (SELECT id FROM public.person WHERE first_name = 'J.R.R.' AND last_name = 'Tolkein'), 2001,'fantasy',4.1, 'movie'    );
INSERT INTO public.movie(title, director, writer, release_year, genre, rating, movie_type) VALUES ('Lord of the Rings: The Two Towers',(SELECT id FROM public.person WHERE first_name = 'Peter' AND last_name = 'Jackson'), (SELECT id FROM public.person WHERE first_name = 'J.R.R.' AND last_name = 'Tolkein'), 2002,'fantasy', 4.8, 'movie');
INSERT INTO public.movie(title, director, writer, release_year, genre, rating, movie_type) VALUES ('Lord of the Rings: Return of the King',(SELECT id FROM public.person WHERE first_name = 'Peter' AND last_name = 'Jackson'), (SELECT id FROM public.person WHERE first_name = 'J.R.R.' AND last_name = 'Tolkein'), 2003,'fantasy', 4.5, 'movie');

-- insert values to connection table
INSERT INTO public.movie_main_stars(movie_id, main_stars_id) VALUES ((SELECT id FROM public.movie WHERE title='Lord of the Rings: The Fellowship of the Ring'), (SELECT id FROM public.person WHERE first_name = 'J.R.R.' AND last_name = 'Tolkein'));
INSERT INTO public.movie_main_stars(movie_id, main_stars_id) VALUES ((SELECT id FROM public.movie WHERE title='Lord of the Rings: The Two Towers'), (SELECT id FROM public.person WHERE first_name = 'J.R.R.' AND last_name = 'Tolkein'));
INSERT INTO public.movie_main_stars(movie_id, main_stars_id) VALUES ((SELECT id FROM public.movie WHERE title='Lord of the Rings: Return of the King'), (SELECT id FROM public.person WHERE first_name = 'J.R.R.' AND last_name = 'Tolkein'));



