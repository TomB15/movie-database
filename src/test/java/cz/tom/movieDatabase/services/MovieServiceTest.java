package cz.tom.movieDatabase.services;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cz.tom.movieDatabase.entities.Movie;
import cz.tom.movieDatabase.entities.Person;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MovieServiceTest
{
    @Autowired
    private MovieService movieService;

    @Test
    public void testCreateMovie()
    {
        final Movie movieToBeCreated = new Movie(
            "TestMovie",
            new Person(
                "Iam ",
                "TestDirector"
            ),
            new Person(
                "Iam ",
                "TestWriter"
            ),
            1970,
            Arrays.asList(
                new Person(
                "Iam ",
                "TestWriter")
             ),
            "Sci-Fi",
            0.1f,
            "Series"
        );

        final Movie createdMovie = movieService.createMovie(movieToBeCreated);

        Assert.assertNotNull("Movie was not created.",  createdMovie);
    }

    @Test
    public void testGetAllMovie()
    {
        final List<Movie> movies = movieService.getAllMovies();

        Assert.assertFalse("Return value is empty.", movies.isEmpty());
    }

    @Test
    public void testGetMovie()
    {
        final Optional<Movie> movie = movieService.getMovie(1);

        Assert.assertFalse("Return value is empty.", movie.isEmpty());
    }
}
