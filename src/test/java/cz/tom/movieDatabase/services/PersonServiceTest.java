package cz.tom.movieDatabase.services;

import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cz.tom.movieDatabase.entities.Person;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PersonServiceTest
{
    @Autowired
    private PersonService personService;

    @Test
    public void testCreatePerson()
    {
        final Person personToBeCreated = new Person(
            "Iam",
            "TestPerson"
        );

        final Person createdPerson = personService.createPerson(personToBeCreated);

        Assert.assertNotNull("Person was not created.", createdPerson);
    }

    @Test
    public void testGetAllPersons()
    {
        final List<Person> persons = personService.getAllPerson();

        Assert.assertFalse("Return value is empty.", persons.isEmpty());
    }

    @Test
    public void testGetPerson()
    {
        final Optional<Person> person = personService.getPerson(1);

        Assert.assertFalse("Return value is empty.", person.isEmpty());
    }
}
